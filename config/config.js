module.exports = {
    "development": {
        username: "root",
        password: "",
        database: "merlin",
        timezone: "America/Bogota",
        host: "127.0.0.1",
        dialect: "postgres",
        port_api: 3200,
        SECRET:"ai9e5h58h4f6k00",
    },
    "test": {
        "username": "root",
        "password": "merqueo_test",
        "database": "merlin",
        "timezone": "America/Bogota",
        "host": "127.0.0.1",
        "dialect": "postgres"
    },
    "production": {
        username: process.env.RDS_USERNAME,
        password: process.env.RDS_PASSWORD,
        database: process.env.RDS_DATABASE,
        host: process.env.RDS_HOST,
        dialect: "postgres",
        timezone: process.env.TIME_ZONE,
        port_api: process.env.PORT_API,
        SECRET:process.env.SECRET,
    }
}