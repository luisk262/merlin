import {GraphQLSchema} from 'graphql';
import 'graphql-import-node';
import { makeExecutableSchema } from 'graphql-tools';
import {fileLoader,mergeTypes} from 'merge-graphql-schemas';


import resolvers from './../resolvers/resolversMap';
const typeDefs = mergeTypes(fileLoader(`${__dirname}/**/*.graphql`),{all:true});

const schema : GraphQLSchema = makeExecutableSchema({
    resolvers,
    typeDefs
});
export default schema;