import {IResolvers} from "graphql-tools";
import PaymentService from "../../services/paymentService";

const newPaymentMutation: IResolvers = {
    Mutation: {
        async newPaymentMutation(__: void, {payment}, context): Promise<any> {
            const paymentService = new PaymentService();
            return paymentService.newPayment(payment);
        }
    }
}
export default newPaymentMutation;
