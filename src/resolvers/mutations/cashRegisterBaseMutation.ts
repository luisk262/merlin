import {IResolvers} from "graphql-tools";
import CashService from "../../services/cashService";

const cashRegisterBaseMutation: IResolvers = {
    Mutation: {
        async cashRegisterBaseMutation(__: void, {cash_base}, context): Promise<any> {

            const cashService = new CashService();
            const transactionResponse = await cashService.newTransaction().then(async (transactionResult: any) => {
                await cash_base.forEach((money: any) => {
                    const cashMovement = {
                        transaction_id: transactionResult.transaction_id,
                        money_id: money.money_id,
                        units: money.units,
                        created_at: Date.now(),
                        updated_at: Date.now()
                    };
                    context.models.cash_movement.create(cashMovement).then((result: any) => {
                        return true;
                    });
                });
                return {
                    success: true,
                    message: "Registro relizado."
                };
            });

            return transactionResponse;
        }
    }
}
export default cashRegisterBaseMutation;
