import {IResolvers} from "graphql-tools";
import PaymentService from "../../services/paymentService";
import CashService from "../../services/cashService";

const emptyBoxMutation: IResolvers = {
    Mutation: {
        async emptyBoxMutation(__: void, {payment}, context): Promise<any> {
            const cashService = new CashService();
            cashService.emptyBoxEgress().then((result: any) => {
                console.log('Caja vaciada..');
            });
            return {
                success: true,
                message: "La caja quedo vacia."
            };
        }
    }
}
export default emptyBoxMutation;
