import {mergeResolvers} from "merge-graphql-schemas";

//----------- Mutations -------------//
import cashRegisterBaseMutation from './mutations/cashRegisterBaseMutation';
import newPaymentMutation from "./mutations/newPaymentMutation";
import emptyBoxMutation from "./mutations/emptyBoxMutation";
//------------ Querys ---------------//
import currentCashStatusQuery from "./querys/currentCashStatusQuery";
import historyLogQuery from "./querys/historyQuery";
import moneysQuery from "./querys/moneysQuery";


const resolvers = [
    //--------Querys------------/
    currentCashStatusQuery,
    historyLogQuery,
    moneysQuery,

    //---------Mutations--------/
    cashRegisterBaseMutation,
    newPaymentMutation,
    emptyBoxMutation
];

export default mergeResolvers(resolvers);
