import {IResolvers} from "graphql-tools";
import MoneyService from "../../services/moneyService";

const moneysQuery: IResolvers = {
    Query: {
        async moneysQuery(__: void, {}, context): Promise<any> {
            const moneyService = new MoneyService();
            return moneyService.getMoneys();
        }
    }
};
export default moneysQuery;
