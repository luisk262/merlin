import {IResolvers} from "graphql-tools";
import DefaultService from "../../services/defaultService";

const historyQuery: IResolvers = {
    Query: {
        async historyLogQuery(__: void, {startTime, endTime}, context): Promise<any> {
            const defaultService = new DefaultService();
            return defaultService.getHistoryLog(startTime, endTime);

        }
    }
};
export default historyQuery;
