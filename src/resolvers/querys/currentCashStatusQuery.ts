import {IResolvers} from "graphql-tools";
import CashService from "../../services/cashService";

const currentCashStatusQuery: IResolvers = {
    Query: {
        async currentCashStatusQuery(__: void, {}, context): Promise<any> {
            const respond = {
                cash: [],
                total_cash: 0
            };
            const cashService = new CashService();
            const currentCash = await cashService.getCurrentCash().then(async (currentCash: any) => {
                return currentCash;
            });

            let total = 0;
            if (currentCash.length) {
                await currentCash.forEach(function (currentCash: any) {
                    total += currentCash.units * currentCash.money.value;
                });

                respond.cash = currentCash;
                respond.total_cash = total;
            }

            return respond;
        }
    }
};
export default currentCashStatusQuery;
