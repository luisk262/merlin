module.exports = function (sequelize, DataTypes) {
    return sequelize.define('money', {
        money_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        money_type_id: {
            type: DataTypes.BIGINT,
            allowNull: false,
            references: {
                model: 'money_type',
                key: 'money_type_id'
            }
        },
        value: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        priority: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
    }, {
        tableName: 'money',
        freezeTableName: true,
        underscored: true
    });
};
