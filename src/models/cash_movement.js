module.exports = function (sequelize, DataTypes) {
    return sequelize.define('cash_movement', {
        cash_movement_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        transaction_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'transaction',
                key: 'transaction_id'
            }
        },
        money_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'money',
                key: 'money_id'
            }
        },
        units: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
    }, {
        tableName: 'cash_movement',
        freezeTableName: true,
        underscored: true
    });
};
