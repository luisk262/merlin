module.exports = function (sequelize, DataTypes) {
    return sequelize.define('entrance_payment', {
            entrance_payment_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            payment_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'payment',
                    key: 'payment_id'
                }
            },
            money_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'money',
                    key: 'money_id'
                }
            },
            units: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
        },
        {
            tableName: 'entrance_payment',
            freezeTableName: true,
            underscored: true
        }
    )

};
