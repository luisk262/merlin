module.exports = function (sequelize, DataTypes) {
    return sequelize.define('money_type', {
        money_type_id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        name: {
            type: "VARCHAR(20)",
            allowNull: false
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
    }, {
        tableName: 'money_type',
        freezeTableName: true,
        underscored: true
    });
};
