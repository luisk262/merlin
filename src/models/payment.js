module.exports = function (sequelize, DataTypes) {
    return sequelize.define('payment', {
            payment_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: "VARCHAR(20)",
                allowNull: false
            },
            value: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
        },
        {
            tableName: 'payment',
            freezeTableName: true,
            underscored: true
        }
    )

};
