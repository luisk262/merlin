module.exports = function (sequelize, DataTypes) {
    return sequelize.define('transaction_type', {
            transaction_type_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: "VARCHAR(20)",
                allowNull: false
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: true,
                defaultValue: sequelize.literal('NOW()')
            },
        },
        {
            tableName: 'transaction_type',
            freezeTableName: true,
            underscored: true
        }
    )

};
