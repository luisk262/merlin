/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('transaction', {
        transaction_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        transaction_type_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'transaction_type',
                key: 'transaction_type_id'
            }
        },
        payment_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'payment',
                key: 'payment_id'
            }
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('NOW()')
        },
    }, {
        tableName: 'transaction',
        freezeTableName: true,
        underscored: true
    });
};
