'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('money_type', [
        {name: 'Billete'},
        {name: 'Moneda'}
      ]);
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('money_type', null, {});
  }
};
