'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('transaction_type', [
        {name: 'Ingreso Base'},
        {name: 'Ingreso Comun'},
        {name: 'Egreso'}
      ], {});
  },
  
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('transaction_type', null, {});
  }
};
