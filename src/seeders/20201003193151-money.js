'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('money', [
        {money_type_id: 1,value:100000,priority:1},
        {money_type_id: 1,value:50000,priority:2},
        {money_type_id: 1,value:20000,priority:3},
        {money_type_id: 1,value:10000,priority:4},
        {money_type_id: 1,value:5000,priority:5},
        {money_type_id: 1,value:1000,priority:6},
        {money_type_id: 2,value:500,priority:7},
        {money_type_id: 2,value:200,priority:8},
        {money_type_id: 2,value:100,priority:9},
        {money_type_id: 2,value:50,priority:10},
      ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('money', null, {});
  }
};
