import MoneyService from "./moneyService";

const TransactionModel = require('./../models').transaction;
const TransactionTypeModel = require('./../models').transaction_type;
const CashMovementModel = require('./../models').cash_movement;
const MoneyModel = require('./../models').money;

import sequialize from 'sequelize';

const Op = sequialize.Op;

import moment from "moment";


class DefaultService {

    async getFullValue(cashObjects: any) {
        const moneyService = new MoneyService();
        let total = 0;
        await moneyService.getMoneys().then((moneys: any) => {
            cashObjects.forEach((cash: any) => {
                const element = moneys.find((moneyElement: any) => moneyElement.money_id === cash.money_id);
                total += element.value * cash.units;
            });
        });
        return total;
    }

    async getHistoryLog(startTime = null, endTime = null) {

        TransactionModel.belongsTo(TransactionTypeModel, {
            foreignKey: 'transaction_type_id',
            sourceKey: 'transaction_type_id'
        });
        TransactionModel.hasMany(CashMovementModel, {
            foreignKey: 'transaction_id',
            sourceKey: 'transaction_id'
        });
        CashMovementModel.belongsTo(MoneyModel, {
            foreignKey: 'money_id',
            sourceKey: 'money_id'
        });
        let query: any = {};

        if (startTime && endTime) {
            const start = moment(startTime).format();
            const end = moment(endTime).format();
            query.created_at = {[Op.gt]: start, [Op.lte]: end}
        }

        const history = await TransactionModel.findAll({
            where: query,
            include: [
                {model: TransactionTypeModel},
                {
                    model: CashMovementModel, include: [
                        {model: MoneyModel}]
                }
            ],
            order: [['createdAt', 'DESC']]
        }).then((historyResult: any) => {
            return historyResult;
        });
        return history;
    }
}

export default DefaultService;
