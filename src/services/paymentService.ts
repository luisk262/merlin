const PaymentModel = require('./../models').payment;
const EntrancePaymentModel = require('./../models').entrance_payment;
import DefaultService from "./defaultService";
import CashService from "./cashService";

class PaymentService {
    async newPayment(payment: any) {
        let result = {
            success: false,
            message: '',
            relay: {},
        };
        const defaultService = new DefaultService();
        await defaultService.getFullValue(payment.cash).then(async (totalCash: number) => {
            const repay = totalCash - payment.value;

            if (repay < 0) {
                result['message'] = 'Te falta dinero para realizar la compra';
            } else {
                await this.validateCash(repay).then(async (validateResult: boolean) => {
                    if (validateResult) {
                        const cashService = new CashService();
                        await cashService.generateRelay(payment.cash, repay).then(async (relayRespond: any) => {
                            if (relayRespond.remainder == 0) {
                                const paymentResult = await this.registerPayment(payment).then((paymentResult: any) => {
                                    return paymentResult;
                                });
                                console.log('id del nuevo pago.................', paymentResult.payment_id)
                                const cashService = new CashService();
                                await cashService.newEntry(relayRespond.newCash,2 , paymentResult.payment_id).then((newEgressResult: boolean) => {
                                    console.log('Nuevo saldo de caja actualizado');
                                });

                                result['success'] = true;
                                result['message'] = 'Pago Exitoso';
                                result['relay'] = {
                                    cash: relayRespond.relay,
                                    total_cash: relayRespond.total_cash
                                };
                                console.log('Generando respuesta', result);
                            } else {
                                result['message'] = 'No tienes dinero para dar vueltas';
                            }
                        });

                    } else {
                        result['message'] = 'En el momento no tenemos,suficiente dinero para dar las vueltas';
                    }
                    return validateResult;
                });

            }
        });

        return result;
    }

    async validateCash(repay: number) {
        let result = true;
        const cashService = new CashService();
        const currentCash = await cashService.getCurrentCash().then((currentCash: any) => {
            return currentCash;
        });

        let total = 0;

        await currentCash.forEach(function (currentCash: any) {
            total += currentCash.units * currentCash.money.value;
        });
        if (repay > total) {
            result = false;
        }
        return result;
    }

    async registerPayment(payment: any) {
        return PaymentModel.create({name: payment.name, value: payment.value}).then(async (newPaymentResult: any) => {
            await payment.cash.forEach((entrancePayment: any) => {
                EntrancePaymentModel.create({
                    payment_id: newPaymentResult.payment_id,
                    money_id: entrancePayment.money_id,
                    units: entrancePayment.units
                }).then((newEntrancePayment: any) => {
                    console.log('Nueva pago ingresado');
                });
            });
            return newPaymentResult;
        }).catch((e: any) => {
            console.log(e);
            return false;
        });
    }

}

export default PaymentService;
