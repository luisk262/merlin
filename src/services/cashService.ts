import MoneyService from "./moneyService";

const MoneyModel = require('./../models').money;
const TransactionModel = require('./../models').transaction;
const CashMovementModel = require('./../models').cash_movement;


class CashService {
    async getCurrentCash() {
        let result = false;
        const transaction = await TransactionModel.findOne({order: [['created_at', 'DESC']]}).then((transactionRespond: any) => {
                return transactionRespond.dataValues;
            }
        ).catch((e: any) => {
            console.log('Error', e);
        });
        if (transaction) {
            let query: any = {};
            query.transaction_id = transaction.transaction_id;

            CashMovementModel.belongsTo(MoneyModel, {foreignKey: 'money_id', sourceKey: 'money_id'});

            result = await CashMovementModel.findAll({
                where: query, include: [{
                    model: MoneyModel
                }]
            }).then((responseReserve: any) => {
                return responseReserve;
            });
        }
        return result;

    }

    async generateRelay(cashes: any, relay: number) {
        let relayReponse = {
            relay: [{}],
            newCash: [{}],
            remainder: relay,
            total_cash: relay
        };
        let remainder = relay;

        const snapshotCash = await this.getCurrentCash().then((currentCash: any) => {
            return currentCash;
        });

        const moneyService = new MoneyService();
        const moneys = await moneyService.getMoneys().then((moneys: any) => {
            return moneys;
        });
        relayReponse.relay = [];
        relayReponse.newCash = [];

        await moneys.forEach((money: any) => {
            let cash = null;

            const currentCash = snapshotCash.find((snapshotMoney: any) => snapshotMoney.money_id === money.money_id);
            const entranceCash = cashes.find((entranceMoney: any) => entranceMoney.money_id === money.money_id);
            if (currentCash && entranceCash) {
                cash = {money_id: money.money_id, value: money.value, units: currentCash.units + entranceCash.units}
            } else if (currentCash) {
                cash = {money_id: money.money_id, value: money.value, units: currentCash.units}
            } else if (entranceCash) {
                cash = {money_id: money.money_id, value: money.value, units: entranceCash.units}
            }

            if (cash) {
                if (relay > 0) {
                    if (remainder >= cash.value) {
                        console.log('remanente:' + remainder + ' ValorMoneda:' + cash.value);
                        if (cash.units > 0) {
                            let count = 0;
                            while (remainder >= cash.value) {
                                count++;
                                cash.units--;
                                remainder = remainder - cash.value;
                                if (cash.units == 0) {
                                    console.log('sale while');
                                    break;
                                }
                            }
                            relayReponse.remainder = remainder;
                            const moneyObject = MoneyModel.findOne({where: {money_id: money.money_id}}).then((money: any) => {
                                return money
                            });
                            relayReponse.relay.push({money: moneyObject, money_id: money.money_id, units: count})
                        }
                    }
                }
                relayReponse.newCash.push(cash);
            }
        });
        console.log('Validando si hay dinero para dar vueltas', relayReponse);

        return relayReponse;


    }

    async newEntry(newCash: any[], type: number = 2, paymentId = null) {
        await this.newTransaction(type, paymentId).then(async (transactionResult: any) => {
            newCash.forEach((cash: any) => {
                CashMovementModel.create({
                    transaction_id: transactionResult.transaction_id,
                    money_id: cash.money_id,
                    units: cash.units,
                    created_at: Date.now(),
                    updated_at: Date.now()
                }).then((result: any) => {
                    console.log('registrando entrada');
                });
            });
        });
        return true;
    }

    async emptyBoxEgress() {
        const moneyService = new MoneyService();
        let cashList: any[] = [];
        await moneyService.getMoneys().then((moneys: any) => {
            moneys.forEach((money: any) => {
                cashList.push({money_id: money.money_id, units: 0});
            })
            this.newEntry(cashList, 3);
        });
        return true;
    }

    newTransaction(type: number = 1, paymentId = null) {
        const transaction = {
            transaction_type_id: type,
            payment_id: paymentId,
            created_at: Date.now(),
            updated_at: Date.now()
        };
        return TransactionModel.create(transaction).then((transactionResult: any) => {
            return transactionResult;
        });
    }
}

export default CashService;
