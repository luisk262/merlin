const MoneyModel = require('./../models').money;

class MoneyService {
    getMoneys() {
        return MoneyModel.findAll().then((moneysResult: any) => {
            return moneysResult;
        })
    }
}

export default MoneyService;
