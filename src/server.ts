
import express from 'express';
import schema from './schema';
import environments from '../config/environments';
import { ApolloServer} from 'apollo-server-express';
import { createServer } from 'http';

// Models
const models = require('./models');

if(process.env.NODE_ENV !=='production'){
    const envs = environments;
}

async function init(){
    const app = express();

    models.sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch(() => {
      console.error('Unable to connect to the database..');
    });

    const server = new ApolloServer({
      schema,
      context: request =>{
        return {
          ...request,
          models,
        }
      },
      introspection:true
    });
    server.applyMiddleware({app});
    const httpServer = createServer(app);

    const PORT =  process.env.PORT || 3200;
    httpServer.listen(
      {port:PORT},
      ()=>console.log(`server running in port http://localhost:${PORT}/graphql`)
    );
}
init();


