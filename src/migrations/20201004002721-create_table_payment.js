'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('payment', {
        payment_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: "VARCHAR(20)",
          allowNull: false
        },
        value: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        created_at: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: true,
          type: Sequelize.DATE
        }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('payment');
  }
};
