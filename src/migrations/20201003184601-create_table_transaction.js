'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('transaction', {
                transaction_id: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    primaryKey: true,
                    autoIncrement: true
                },
                transaction_type_id: {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    references: {
                        model: 'transaction_type',
                        key: 'transaction_type_id'
                    }
                },
                created_at: {
                    allowNull: true,
                    type: Sequelize.DATE
                },
                updated_at: {
                    allowNull: true,
                    type: Sequelize.DATE
                }
            }
        );
    },

    down: (queryInterface, Sequelize) => {

        return queryInterface.dropTable('transaction');

    }
};
