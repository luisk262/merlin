'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('money', {
            money_id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            money_type_id: {
                type: Sequelize.BIGINT,
                allowNull: false,
                references: {
                    model: 'money_type',
                    key: 'money_type_id'
                }
            },
            value: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            priority: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            created_at: {
                allowNull: true,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: true,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('money');
    }
};
