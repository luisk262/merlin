'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('cash_movement', {
            cash_movement_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            transaction_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'transaction',
                    key: 'transaction_id'
                }
            },
            money_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'money',
                    key: 'money_id'
                }
            },
            units: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            created_at: {
                allowNull: true,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: true,
                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('cash_movement');
    }
};
