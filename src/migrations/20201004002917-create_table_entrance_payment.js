'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('entrance_payment', {
        entrance_payment_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true
        },
        payment_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'payment',
            key: 'payment_id'
          }
        },
        money_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'money',
            key: 'money_id'
          }
        },
        units: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        created_at: {
          allowNull: true,
          type: Sequelize.DATE
        },
        updated_at: {
          allowNull: true,
          type: Sequelize.DATE
        }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('entrance_payment');
  }
};
