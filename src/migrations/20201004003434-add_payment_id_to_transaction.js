'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'transaction',
            'payment_id',
            {
                type: Sequelize.INTEGER,
                references: {
                    model: 'payment',
                    key: 'payment_id'
                },
                allowNull: true,
                defaultValue: null
            });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('transaction', 'payment_id');
    }
};
