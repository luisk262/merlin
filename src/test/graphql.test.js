const chai = require('chai');

const expect = chai.expect;
const url = `http://localhost:3200`;
const request = require('supertest')(url);

describe('GraphQL', () => {

    it('Returns all moneys', (done) => {
        request.post('/graphql')
            .send({query: '{ moneysQuery{ money_id value} }'})
            .expect(200)
            .end((err, res) => {
                // res will contain array with one user
                if (err) return done(err);
                expect(res.body.data.moneysQuery).to.have.length(10)
                done();
            })
    })
    it('Create base Cash', (done) => {
        request.post('/graphql')
            .send({
                query: 'mutation{' +
                    'cashRegisterBaseMutation(cash_base:[' +
                    '    {money_id:3,units:5},' +
                    '    {money_id:4,units:10},' +
                    '    {money_id:7,units:15},' +
                    '    {money_id:8,units:20}' +
                    '  ]){' +
                    '    success' +
                    '    message' +
                    '  }}'
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                expect(res.body.data.cashRegisterBaseMutation).to.have.property('success').to.be.ok
                done();
            })
    })

    it('Current status Cash', (done) => {
        request.post('/graphql')
            .send({
                query: '{ currentCashStatusQuery{' +
                    '    cash{' +
                    '      units' +
                    '      money{' +
                    '        money_id' +
                    '        value' +
                    '      }' +
                    '    }' +
                    '    total_cash' +
                    '  } }'
            })
            .expect(200)
            .end((err, res) => {
                // res will contain array with one user
                if (err) return done(err);
                expect(res.body.data.currentCashStatusQuery.total_cash).to.equal(211500)
                done();
            })
    })
    it('New Payment', (done) => {
        request.post('/graphql')
            .send({
                query: 'mutation{newPaymentMutation(payment:{' +
                    '    name:"1 Lapiz",' +
                    '    value:10000' +
                    '    cash:[' +
                    '      {money_id:2,units:1},' +
                    '   ]' +
                    '  }){' +
                    '       success' +
                    '       message' +
                    '    relay{' +
                    '      cash{' +
                    '        money{' +
                    '          value' +
                    '        }' +
                    '        units' +
                    '      }' +
                    '      total_cash' +
                    '    }' +
                    '}}'
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                expect(res.body.data.newPaymentMutation).to.have.property('success').to.be.ok;
                expect(res.body.data.newPaymentMutation.relay).to.have.property('total_cash').to.equal(40000);
                done();
            })
    })
    it('emptyBox Box', (done) => {
        request.post('/graphql')
            .send({
                query: 'mutation{emptyBoxMutation{' +
                    '    success' +
                    '    message' +
                    '  }}'
            })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                expect(res.body.data.emptyBoxMutation).to.have.property('success').to.be.ok;
                done();
            })
    })


});
