# Instalación de Merlin 

Prueba Técnica Backend Merqueo<br>
* Por: Luis Carlos Alvarez 
* Email: Luisk__@hotmail.com <br>
* Tel: (+57)3114579556

# Requerimientos
* Instalar la base de datos PostgreSQL 12.4 si esta en un computador cliente con osx, windows o linux descarge postresapp http://postgresapp.com/documentation/all-versions.html.
* Cree el usuario de base de datos `user:root` `password:vacio`  createuser -s root
* Instalar Node v10.22.0 (LTS) https://nodejs.org/es/download/releases/
* Instalar todos los modulos `npm i` dentro del proyecto
* Si los pasos anteries fueron correctos puede crear la base de datos ejecutando en consola  `node_modules/.bin/sequelize db:create`
* Ejecute `node_modules/.bin/sequelize db:migrate` para crear la estructura de base de datos requerida puede verificar su esquema aqui https://lucid.app/documents/view/7f1e5f33-07ae-4039-94cc-327d513af75b/0_0#?folder_id=home&browser=icon
* Ejectute `node_modules/.bin/sequelize db:seed:all` para completar los datos paramentricos minimos de base de datos	
* Si tiene algun problema en el esquema, o datos parametricos puede reiniciar la base de datos con `node_modules/.bin/sequelize db:migrate:undo:all` y repetir los dos ultimos pasos

## Ejecutar el servidor

* Desde El directorio del proyecto clonado ejecute npm `npm run start:api` podra ver el servidor corriendo en http://localhost:3200/graphql

## Ejecutar pruebas

Ejecute `npm test` para ejecutar pruebas.
1) Returns all moneys
2) Create base Cash
3) Current status Cash
4) New Payment
5) emptyBox Box

# GraphQL end Points
Si ejecuta el servidor local mente podra ver en el costado derecho la documentacion necesaria para consumir cada endPoint.

Acontinuación se mencionan los puntos disponibles en la API
##Mutaciones
* `cashRegisterBaseMutation(cash_base:[MoneyInput!]):Default!`
* `newPaymentMutation(payment:PaymentInput!):PaymentOutput!`
* `emptyBoxMutation:Default!`
##Queries
* `currentCashStatusQuery:StatusOutput!`
* `historyLogQuery(startTime:String,endTime:String):[History]`
* `moneysQuery:[Money]`

De igual Manera dejo un ejemplo en el mismo repositorio archivo `example_graphql.txt`

##Demo 
Consulte la ruta http://merlin-env.eba-yckqgxqv.us-west-2.elasticbeanstalk.com/graphql
para probar la API puede usar la herramienta https://github.com/graphql/graphql-playground



